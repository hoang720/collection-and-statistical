import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThongKeRoutingModule } from './thong-ke-routing.module';
import { ThongKeComponent } from './thong-ke.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgApexchartsModule } from "ng-apexcharts";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    ThongKeComponent
  ],
  imports: [
    CommonModule,
    ThongKeRoutingModule,
    NgxPaginationModule,
    NgApexchartsModule,
    FormsModule,ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class ThongKeModule { }
