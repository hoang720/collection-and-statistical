import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ThongKeService } from "../../core/services/thong-ke.service"

@Component({
  selector: 'app-thong-ke',
  templateUrl: './thong-ke.component.html',
  styleUrls: ['./thong-ke.component.scss']
})
export class ThongKeComponent implements OnInit {
  sourceData: any;
  jobData: any;
  pieChart1Options: any;
  pieChart2Options: any;
  dataSource = new MatTableDataSource<any>();
  @ViewChild('paginator') paginator: any;
  displayedColumns: string[] = [
    "Số thứ tự",
    "Name",
    "Min",
    "Max",
    "Avg",
  ]

  jobTotal: any;
  companyTotal: any;

  constructor(
    private thongKeService: ThongKeService
  ) { }

  ngOnInit(): void {
    this.getSource()
    this.getJob()
    this.getJobAndCompany()
    this.getSalary()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  formatArray(item: any){
    let arr = []
    for (var i in item) {
      arr.push(item[i])
    }
    
  }

  getJobAndCompany(){
    this.thongKeService.getJobAndCty().subscribe(data => {
      this.companyTotal = data.total_company
      this.jobTotal = data.total_job
    })
  }
  getSalary(){
    this.thongKeService.getSalary().subscribe(data => {
      this.dataSource.data = data
    })
  }
  getSource(){
    this.thongKeService.getSources().subscribe(data => {
      this.sourceData = []
      for (var i in data){
        this.pieChart1Options = {
          series: [data[i].count],
          chart: {
            width: 580,
            type: 'pie',
          },
          labels: [data[i]._id],
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        };
        console.log(data[i])
      }
    })
  }

  getJob(){
    this.thongKeService.getJobQuantity().subscribe(data => {
      this.jobData = []
      for (var i in data){
        this.pieChart2Options = {
          series: [19, 0, 2, 0, 0, 0, 0, 0],
          chart: {
            width: 600,
            type: 'pie',
          },
          labels: ["Công nghệ thông tin", "Kế toán", "Kinh doanh", "Chứng khoán", "Cơ khí", "Giao vận", "Y tế", "Xây dựng"],
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        };
        console.log(data[i])
        this.sourceData.push(this.pieChart1Options);
      }
    })
  }
}
