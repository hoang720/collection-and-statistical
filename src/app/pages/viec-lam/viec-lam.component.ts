import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { JobService } from 'src/app/core/services/job.service';

@Component({
  selector: 'app-viec-lam',
  templateUrl: './viec-lam.component.html',
  styleUrls: ['./viec-lam.component.scss']
})
export class ViecLamComponent implements OnInit {
  dataSource = new MatTableDataSource<any>();
  page = 1;
  page_size = 10;
  totalElements = 0;
  @ViewChild('paginator') paginator: any;
  optionCompany: any;
  optionCareer: any;
  displayedColumns: string[] = [
    "JobName",
    "CtyName",
    "Salary",
    "Date",
    "Source",
  ]
  constructor(
    private jobService: JobService,
  ) { }

  ngOnInit(): void {
    this.getAllJob()
    this.getOptionCompany()
    this.getCareer()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getAllJob(){
    this.jobService.getJob(this.page, this.page_size).subscribe(data => {
      this.dataSource.data = data.data
      this.totalElements = data.total
    })
  }
  getOptionCompany(){
    this.jobService.getJobAndCty().subscribe(data => {
      this.optionCompany = data
    })
  }
  getCareer(){
    this.jobService.getCareer().subscribe(data => {
      this.optionCareer = data
      console.log(this.optionCareer)
    })
  }
}
