import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViecLamRoutingModule } from './viec-lam-routing.module';
import { ViecLamComponent } from './viec-lam.component';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    ViecLamComponent
  ],
  imports: [
    CommonModule,
    ViecLamRoutingModule,
    FormsModule,ReactiveFormsModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule
  ]
})
export class ViecLamModule { }
