import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViecLamComponent } from './viec-lam.component';

const routes: Routes = [{ path: '', component: ViecLamComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViecLamRoutingModule { }
