import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhSachCongTyComponent } from './danh-sach-cong-ty.component';

describe('DanhSachCongTyComponent', () => {
  let component: DanhSachCongTyComponent;
  let fixture: ComponentFixture<DanhSachCongTyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanhSachCongTyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DanhSachCongTyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
