import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DanhSachCongTyComponent } from './danh-sach-cong-ty.component';

const routes: Routes = [{ path: '', component: DanhSachCongTyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DanhSachCongTyRoutingModule { }
