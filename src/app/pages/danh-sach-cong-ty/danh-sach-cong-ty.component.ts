import { Component, OnInit } from '@angular/core';
import { ListCompanyService } from 'src/app/core/services/list-company.service';


@Component({
  selector: 'app-danh-sach-cong-ty',
  templateUrl: './danh-sach-cong-ty.component.html',
  styleUrls: ['./danh-sach-cong-ty.component.scss']
})
export class DanhSachCongTyComponent implements OnInit {
  p: any;
  companyList: any;
  constructor(
    private companyService: ListCompanyService,
  ) { }

  ngOnInit(): void {
    this.getAllcompany()
  }

  getAllcompany(){
    this.companyService.getJobAndCty().subscribe(data => {
      this.companyList = data
    })
  }
}
