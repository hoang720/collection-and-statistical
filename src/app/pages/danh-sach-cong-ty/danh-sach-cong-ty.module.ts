import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DanhSachCongTyRoutingModule } from './danh-sach-cong-ty-routing.module';
import { DanhSachCongTyComponent } from './danh-sach-cong-ty.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    DanhSachCongTyComponent
  ],
  imports: [
    CommonModule,
    DanhSachCongTyRoutingModule,
    NgxPaginationModule
  ]
})
export class DanhSachCongTyModule { }
