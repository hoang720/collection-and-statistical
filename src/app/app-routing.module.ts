import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'admin', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule) }, 
  { path: '', loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginModule) },
  { path: 'thong-ke', loadChildren: () => import('./pages/thong-ke/thong-ke.module').then(m => m.ThongKeModule) },
  { path: 'danh-sach-cong-ty', loadChildren: () => import('./pages/danh-sach-cong-ty/danh-sach-cong-ty.module').then(m => m.DanhSachCongTyModule) },
  { path: 'viec-lam', loadChildren: () => import('./pages/viec-lam/viec-lam.module').then(m => m.ViecLamModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
