import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ListCompanyService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getJobAndCty(): Observable<any>{
    let apiURL = `${this.url}/jobs/company-list/`;
    return this.http.get<any>(apiURL)
  }
}
