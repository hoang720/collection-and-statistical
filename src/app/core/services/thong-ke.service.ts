import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ThongKeService {

  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getJobAndCty(): Observable<any>{
    let apiURL = `${this.url}/jobs/info/`;
    return this.http.get<any>(apiURL)
  }
  getSources(): Observable<any>{
    let apiURL = `${this.url}/jobs/sources-list/`;
    return this.http.get<any>(apiURL)
  }
  getJobQuantity(): Observable<any>{
    let apiURL = `${this.url}/jobs/career-list/quantity-stats`;
    return this.http.get<any>(apiURL)
  }
  getSalary(): Observable<any>{
    let apiURL = `${this.url}/jobs/career-list/salary-stats`;
    return this.http.get<any>(apiURL)
  }
}
