import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class JobService {
  private url = environment.apiURL
  constructor(private http: HttpClient) { }

  getJob(page: any, page_size: any): Observable<any>{
    let apiURL = `${this.url}/jobs?page=${page}&page_size=${page_size}`;
    return this.http.get<any>(apiURL)
  }
  getJobAndCty(): Observable<any>{
    let apiURL = `${this.url}/jobs/company-list/`;
    return this.http.get<any>(apiURL)
  }
  getCareer(): Observable<any>{
    let apiURL = `${this.url}/jobs/career-list/`;
    return this.http.get<any>(apiURL)
  }
}
