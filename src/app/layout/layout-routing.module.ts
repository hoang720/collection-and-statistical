import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  { 
    path: '', component: LayoutComponent,
    children: [
      { path: 'thong-ke', loadChildren: () => import('../pages/thong-ke/thong-ke.module').then(m => m.ThongKeModule) },
      { path: 'danh-sach-cong-ty', loadChildren: () => import('../pages/danh-sach-cong-ty/danh-sach-cong-ty.module').then(m => m.DanhSachCongTyModule) },
      { path: 'viec-lam', loadChildren: () => import('../pages/viec-lam/viec-lam.module').then(m => m.ViecLamModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
