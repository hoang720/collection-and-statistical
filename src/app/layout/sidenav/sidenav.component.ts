import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  ListSide = [
    {icon: 'nav-icon fas fa-bar-chart', name: 'Thống kê', path: 'thong-ke'},
    {icon: 'nav-icon fas fa-building', name: 'Danh sách công ty', path: 'danh-sach-cong-ty'},
    {icon: 'nav-icon fas fa-business-time', name: 'Việc làm', path: 'viec-lam'},
  ]
}
